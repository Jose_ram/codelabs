import { LitElement, html } from 'lit-element' ;
import '../persona-ficha-listado/persona-ficha-listado';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement{

    /*static get styles(){
        return css`
            :host {
                all: initial;
            }
        `;
    }*/

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley"
                }
            }, {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Lorem ipsum.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner"
                }
            }, {
                name: "Eowyn",
                yearsInCompany: 5,
                profile: "Lorem ipsum es el texto que se usa habitualmente en diseño gráfico en demostraciones de tipografías o de borradores de diseño para probar el diseño visual antes de insertar el texto fina",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn"
                }
            }, {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile: "Lorem ipsum, consectetur adipisici elit.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Turanga Leela"
                }
            }, {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile: "Lorem ipsum.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Tyrion Lannister"
                }
            }
        ];

        this.showPersonForm = false;
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`
                    <persona-ficha-listado 
                        fname="${person.name}"
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `;
    }

    updated(changedProperties){
        console.log("Updated");

        if (changedProperties.has("showPersonForm")) {
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true){
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ));
        }
    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");

        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log("La propiedad name en person vale " + e.detail.person.name);
        console.log("La propiedad profile en person vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + e.detail.person.yearsInCompany);
        console.log("La propiedad editingPerson vale " + e.detail.editingPerson);

        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

            this.people= this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );
        /*    let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            if (indexOfPerson >= 0){
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;
            }
            */
        } else {
            console.log("Se va a almacenar una persona nueva");
           // this.people.push(e.detail.person);
           this.people = [...this.people, e.detail.person];
        }

        console.log("Persona almacenada");

        this.showPersonForm = false;
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");

        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se ve a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name 
        )
    }

    infoPerson(e){
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona de nombre " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        console.log(chosenPerson);

        let personToShow = {};
        personToShow.name  = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.tearsInCompany = chosenPerson[0].yearsInCompany;
 
        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }
}

customElements.define('persona-main', PersonaMain);