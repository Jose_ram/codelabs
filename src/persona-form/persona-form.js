import { LitElement, html} from 'lit-element' ;

class PersonaForm extends LitElement{

    static get properties(){
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.resetFormData();

    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" 
                            @input="${this.updateName}" 
                            .value="${this.person.name}" 
                            ?disabled="${this.editingPerson}"
                            class="form-control" 
                            placeholder="Nombre completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name de person con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile de person con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany de person con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    }

    storePerson(e){
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src" : "./img/images.jpg",
            "alt" : "Persona"
        }

        console.log("La propiedad name en person vale " + this.person.name);
        console.log("La propiedad profile en person vale " + this.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + this.person.yearsInCompany);

        this.dispatchEvent(new CustomEvent("persona-form-store", {
                detail: {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson: this.editingPerson
                }
            })
        );
    }

    resetFormData(){
        console.log("resetFormData");

        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm);